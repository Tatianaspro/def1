x = float(input('Введите первое число: '))
y = float(input('Введите второе число: '))
operation = input('Введите знак: ')
result = None


def calcul():
    if operation == '+':
        result = plus(x, y)
    elif operation == '-':
        result = minus(x, y)
    elif operation == '*':
        result = multiply(x, y)
    elif operation == '/':
        if y != 0:
            result = divide(x, y)

    return (result)


def plus(x, y):
    return (x + y)


def minus(x, y):
    return (x - y)


def multiply(x, y):
    return (x * y)


def divide(x, y):
    return (x / y)


print(calcul())
